package calc;

import java.util.Random;

public class Exercice30 {

	public static void exercice30(String nbr) {
		int e = 0, d = 0;
		for (int i = 0; i < nbr.indexOf("."); i++) {
			e += Character.getNumericValue(nbr.charAt(i));
		}
		for (int i = nbr.indexOf(".") + 1; i < nbr.length(); i++) {
			d += Character.getNumericValue(nbr.charAt(i));
		}
		System.out.println("T" + (e + d) + "E" + e + "D" + d);
	}
}