package calc;

import java.util.Random;

public class Exercice28 {
	public static int exo(String str) {

		int res = 0;
		for (int i = 0; i < str.length(); i++) {
			// res = res + (int) (Character.getNumericValue(str.charAt(i)) * Math.pow(2,
			// (str.length() - 1) - i));
			res += Character.getNumericValue(str.charAt(i)) * Math.pow(2, (str.length() - 1) - i);
		}

		return res;
	}

}
