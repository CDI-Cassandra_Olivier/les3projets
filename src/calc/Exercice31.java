package calc;

import java.util.Scanner;

public class Exercice31 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		String saisie = sc.next();
		
		saisie = saisie.toLowerCase();
		
		StringBuilder resultat = new StringBuilder();
		
		for(int i = 0 ; i < saisie.length() ; i++) {
			
			if(!Character.isAlphabetic((saisie.charAt(i)))) {
				resultat.delete(0, resultat.length());
				resultat.append("Erreur : La saisie comporte des chiffres ou des symboles.");
				break;
			}
			else {
				resultat.append(Integer.toString(((int)saisie.charAt(i) - (int)('a') + 1)));
			}
			
		}
		
		System.out.println(resultat.toString());
	}

}
	
	
