package calc;

public class AutreMotsUtils {

	public static String exercice29(int param1, int param2) {
		StringBuilder res = new StringBuilder();

		int nbI = param1;

		if (param1 % 2 == 0) {
			nbI = param1 + 1;

		}

		for (int i = 0; i < param2; i++) {
			nbI = nbI + 2;
			if (i == param2 - 1) {
				res.append(nbI);
			} else {
				res.append(nbI + "*");
			}

		}

		return res.toString();
	}
}
