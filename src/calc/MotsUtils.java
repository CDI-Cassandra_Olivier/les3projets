package calc;

public class MotsUtils {

	public static String inverser(String str) {

		StringBuilder res = new StringBuilder();

		for (int i = str.length() - 1; i >= 0; i--) {
			res.append(str.charAt(i));
		}
		return res.toString();
	}

	public static String caracteresCommuns(String str, String str2) {
		StringBuilder sb = new StringBuilder();

		if (str.length() > str2.length()) {
			String tmp = str2;
			str2 = str;
			str = tmp;
		}

		for (int i = 0; i < str.length(); i++) {
			char seq = str.charAt(i);
			if ((sb.indexOf(seq + "") == -1) && (str2.indexOf(seq) != -1)) {
				sb.append(seq);
			}
		}

		return sb.toString();
	}

	/*
	 * public static boolean estUnPalindrome(String str) { String str2 =
	 * MotsUtils.inverser(str); if (str2.equals(str)) { return true; } else { return
	 * false; } }
	 */

	public static boolean estUnPalindrome1(String str) {

		int k = str.length();

		for (int i = 0, j = k - 1; i < (k - 1) / 2; i++, j--) {
			if (str.charAt(i) != str.charAt(j)) {
				return false;
			}

		}
		return true;
	}

	public static long sommeChiffresDansMot(String str)

	{
		long res = 0;
		for (int i = 0; i < str.length(); i++) {
			if (Character.isDigit(str.charAt(i))) {
				res += Character.digit(str.charAt(i), 10);
			}
		}
		return res;
	}

	public static String doubleAndSoustract(int param) {
		return (param * 2) + "" + (param - 1);
	}

	public static void afficherNombreOccurence(String str) {

		StringBuilder sb = new StringBuilder();
		str = str.toLowerCase();
		char seq;
		int nbSeq;
		int idSeq;

		for (int i = 0; i < str.length(); i++) {
			seq = str.charAt(i);
			nbSeq = 0;
			idSeq = 0;

			if (sb.indexOf(seq + "") != -1)
				continue;

			sb.append(seq);

			do {
				if ((idSeq = str.indexOf(seq, idSeq)) == -1)
					break;
				nbSeq++;
				idSeq++;
			} while (idSeq != -1);
			System.out.println(seq + " " + nbSeq);
		}
	}

	public static void afficherNombreOccurence1(String str) {

		StringBuilder res = new StringBuilder();
		int k = 1;
		int j = 1;
		for (int i = 0; i < str.length(); i++) {
			if (res.indexOf(str.charAt(i) + "") != -1) {
				continue;
			} else {
				for (j = 1; j < str.length(); j++) {
					if (str.charAt(i) == str.charAt(j)) {
						k++;
					}
				}
				res.append(str.charAt(i));
			}
			System.out.println("le nombre d'occurence de " + str.charAt(i) + " est " + k);
			k = 0;
		}

	}

	public static long sommeUnicodes(String str) {
		long res = 0;

		for (int i = 0; i < str.length(); i++) {
			res += (long) str.charAt(i);
		}
		return res;
	}

}
