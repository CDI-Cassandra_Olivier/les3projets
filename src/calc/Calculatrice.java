package calc;


import java.util.Random;

public class Calculatrice {
	public static int addition(int param1, int param2) {
		return param1 + param2;
	}

	public static int soustraction(int parametreX, int parametreY) {
		return parametreX - parametreY;
	}

	public static int multiplication(int parametreX, int parametreY) {
		return parametreX * parametreY;
	}

	public static int max(int parametreX, int parametreY) {
		if (parametreX <= parametreY) {
			return parametreY;
		} else {
			return parametreX;
		}
	}

	public static char signe(int parametreX) {

		if (parametreX < 0) {
			return '-';
		} else if (parametreX == 0) {
			return '0';
		} else {
			return '+';
		}
	}

	public static int factoriel(int parametreX) {
		int res = 1;
		int n = 1;
		for (int i = 1; i < parametreX + 1; i++) {
			res = n * i;
			n = res;
		}

		return res;
	}

	public static int factorielCorrige(int parametreX) {
		int i, res;
		res = 1;
		i = parametreX;
		while (i > 1) {
			res = res * i;
			i--;
		}
		return res;
	}

	public static int factoRecursif(int a) {
		return (a == 0) ? 1 : (a * factoRecursif(a - 1));
	}
	// ?si :sinon

	public static boolean conjonction1(boolean boolA, boolean boolB) {
		boolean res = true;
		if (boolA && boolB) {
			return res;
		} else {
			return res = false;
		}
	}

	public static boolean conjonction(boolean boolA, boolean boolB) {
		return boolA && boolB;
	}

	public static boolean disjonction(boolean boolA, boolean boolB) {
		boolean res = true;
		if (boolA == false || boolB == false) {
			return res = false;
		} else {
			return res;
		}
	}

	public static boolean disjonctioncorrige(boolean boolA, boolean boolB) {
		return boolA || boolB;
	}

	public static boolean xor(boolean boolA, boolean boolB) {
		return (boolA && !(boolB)) || (!(boolA) && boolB);
	}

	public static boolean negation(boolean boolA) {
		return !(boolA);
	}

	public static int aleatoireInferieur(int max) {

		return new Random().nextInt(max);
	}
}
