package calc;

import tools.Clavier;

public class CalculatriceTableau {

	public static int sommeElements(int[] tabInt) {
		int res = 0;

		for (int i = 0; i < tabInt.length; i++) {
			res = res + tabInt[i];
		}

		return res;

	}

	public static int plusPetitElement(int[] tabInt) {
		int res = tabInt[0];

		for (int i = 1; i < tabInt.length; i++) {
			if (res > tabInt[i]) {
				res = tabInt[i];
			}
		}
		return res;
	}

	public static int plusGrandElement(int[] tabInt) {
		int res = tabInt[0];

		for (int i = 1; i < tabInt.length; i++) {
			if (res < tabInt[i]) {
				res = tabInt[i];
			}
		}
		return res;
	}

	public static int valeurMaximum(int[] tabInt) {
		int res = Integer.MIN_VALUE;

		for (int i = 0; i < tabInt.length; i++) {
			if (res < tabInt[i]) {
				res = tabInt[i];
			}
		}
		return res;
	}

	public static long sommeElementsDeuxTableaux(int[] tabInt, int[] tabInt2) {
		int a = 0;

		for (int i = 0; i < tabInt.length; i++) {
			a = a + tabInt[i];
		}

		int b = 0;

		for (int i = 0; i < tabInt2.length; i++) {
			b = b + tabInt2[i];
		}

		long res = a + b;
		return res;
	}

	public static int[] triAscendant(int[] tabInt) {

		int c = 0;

		for (int k = 0; k < tabInt.length; k++) {
			for (int j = 0; j < tabInt.length - 1; j++)

			{
				if (tabInt[j] > tabInt[j + 1]) {
					c = tabInt[j];
					tabInt[j] = tabInt[j + 1];
					tabInt[j + 1] = c;
				}

			}

		}

		return tabInt;
	}

	public static boolean conjonctionet(boolean[] tabBool) {

		for (int i = 0; i < tabBool.length; i++) {
			if (tabBool[i] == false) {
				return false;
			}
		}

		return true;
	}

	public static boolean conjonction(boolean[] tabBool) {
		boolean res = true;
		for (int i = 1; i < tabBool.length; i++) {
			res = res && Calculatrice.conjonction(tabBool[i - 1], tabBool[i]);

		}
		return res;
	}

	public static int[] triAscendantDeuxTableaux(int[] tabInt, int[] tabInt2) {
		int[] res = new int[tabInt.length + tabInt2.length];
		int x = 0;

		for (int i = 0; i < res.length; i++) {
			if (i < tabInt.length) {
				x = tabInt[i];
			} else {
				x = tabInt2[i - tabInt.length];
			}
			res[i] = x;
		}
		res = triAscendant(res);

		return res;
	}

	public static long nombreDElementsPair(int[] tab) {
		long res = 0;
		for (int i = 0; i < tab.length; i++) {
			if (tab[i] % 2 == 0) {
				res = res + 1;
			}
		}
		return res;
	}

	public static boolean chercheSiUnElementExiste(int param, int[] tab) {

		for (int i = 0; i < tab.length; i++) {
			if (param == tab[i]) {
				return true;
			}
		}
		return false;
	}

	public static int[] mettreZeroDansLesCasesAIndicesImpair(int tabInt[]) {
		for (int i = 1; i < tabInt.length; i += 2) {
			tabInt[i] = 0;
		}
		return tabInt;
	}

	public static int[] decalerLesElementsTableauDUneCase(int[] tab) {
		int[] res = new int[tab.length];
		int x = tab[tab.length - 1];
		for (int i = 1; i < res.length; i++) {
			res[i] = tab[i - 1];
		}
		res[0] = x;
		return res;
	}

	public static int[] decalerLesElementsTableauDUneCase1(int[] tab) {
		int temp1 = 0;
		int temp2 = tab[0];
		for (int i = 1; i < tab.length; i++) {
			temp1 = tab[i];
			tab[i] = temp2;
			temp2 = temp1;

			System.out.println(Clavier.contenuTableau(tab));
		}
		tab[0] = temp1;

		return tab;
	}

	public static void programmePlacementAleatoireilot() {
		String[] eleve = { "Maria", "Najim", "Serge", "Cassandra", "Laurent", "Amine", "Karim", "Damien", "Brandon", "Widad","Roman"};
		String[]tete= {"Thomas","Axel","Oussama","Thierry"};
		int ilot=4;
		int place = 0;
		int idRandom = 0;
		String nmEleve = "";
		int random = 0;

		for (int i=0;i<ilot;i++) {
			ilot=ilot+1;
			System.out.println(ilot);
		}

	}
	
	
	
	
	
	
	
	public static void programmePlacementAleatoire() {
		String[] eleve = { "Maria", "Najim", "Serge", "Cassandra", "Laurent", "Amine", "Karim", "Damien", "Brandon",
				"Ossama", "Widad", "homas", "Roman", "Axel", "Thierry" };
		int place = 0;
		int idRandom = 0;
		String nmEleve = "";
		int random = 0;

		for (int i = 0; i < eleve.length; i++) {
			for (int j = 0; j < eleve.length; j++) {

				nmEleve = eleve[i];
				random = Calculatrice.aleatoireInferieur(16);

				if (random != idRandom) {
					place = random;
					idRandom++;
				}
			}
		}
		System.out.println(nmEleve + place + "");

	}
}
