package exec;

import tools.Clavier;

public class Exercice33 {
	// Methode main :
	public static void main(String[] args) {
		System.out.println("*************************************");
		System.out.println(
				"Methode qui prend en parametre une chaine de caract�res et r�alise un calcul avec signes + et - :");
		String str = Clavier.lireString();
		String res = calculString(str);
		System.out.println("Le resultat pour " + str + " est : " + res);
	}

	// Fonction calculString
	public static String calculString(String str) {
		char c = str.charAt(0); // str.charAt(0) == premiere lettre du mot str
		int res = 0;
		boolean positif = true;
		int signe = 0;
		StringBuilder sb = new StringBuilder(); // StringBuilder �quivalent de String mais plus modulable
		for (int i = 0; i < str.length(); i++) { // length() permet de connaitre la longueur d'un String
			sb.setLength(0); // setLength(0) permet de vider le StringBuilder
			if (Character.isDigit(c)) { // Character.isDigit(c) permet de savoir si le char c est un chiffre
				do { // faire jusque la condition while
					sb.append(c); // append(c) permet d'ajouter c au StringBuilder
					if ((i += 1) >= str.length()) { // (i += 1) permet d'incr�menter la valeur de i de 1
													// (�quivalent de i++ ou i = i +1)
						break; // break permet de quitter la boucle (dans ce cas la boucle do{}while();)
					}
					c = str.charAt(i); // c prend la valeur de la lettre suivante
				} while (Character.isDigit(c)); // continuer de faire tant que c est un chiffre
				i--; // permet de d�cr�meter pour ne pas sauter le caract�re qui contient un signe
				if (positif) { // v�rifie la valeur du boolean si il est positif faire :
					res += Integer.valueOf(sb.toString()); // Integer.valueOf(sb.toString()) permet de recuperer la
															// valeur num�rique stocker dans le StringBuilder
				} else { // sinon faire :
					res -= Integer.valueOf(sb.toString()); // Integer.valueOf(sb.toString()) permet de recuperer la
					// valeur num�rique stocker dans le StringBuilder
				}
			} else if (c == '+' || c == '-') { // condition qui v�rifie si le char correspond au signe '+' ou '-'
				do { // faire jusque la condition while
					if (c == '-') { // si c correspond au signe '-' faire :
						signe++; // incrementer la valeur de signe
					}
					if ((i += 1) >= str.length()) { // (i += 1) permet d'incr�menter la valeur de i de 1
													// (�quivalent de i++ ou i = i +1)
						break; // break permet de quitter la boucle (dans ce cas la boucle do{}while();)
					}
					c = str.charAt(i); // c prend la valeur de la lettre suivante
				} while (c == '+' || c == '-'); // continuer de faire tant que c correspond � '+' ou '-'
				i--; // permet de d�cr�meter pour ne pas sauter le caract�re qui contient un signe
				if (signe % 2 == 1) { // condition si signe % 2 == 1 faire :
					positif = false; // mettre la valeur de positif � false
				} else { // sinon faire :
					positif = true; // mettre la valeur de positif � true
				}
				signe = 0; // reinitialiser la valeur de signe � 0
			} else { // si c n'est pas un chiffre, le signe '+' ou '-'
				return "ERROR : String is wrong."; // rentourner la r�ponse "ERROR : String is wrong."
			}
		}
		return res + ""; // retourner la r�ponse valeur de res + ""
		// comme res est un int et la fonction doit retourner un String, le fait
		// d'ajouter un string (vide) au int le transforme en String
	}
}